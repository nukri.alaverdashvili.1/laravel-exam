@extends('layouts.app')

@section('content')
<div class="container">


@foreach ($subject as $subject_v)


<div class="mt-5">
    <h3 class="font-monospace" style="text-decoration:none; color:gray;"> Unhide Your Exam Subjects </h3>

    <a style="display: block;  text-decoration:none; color:gray;" href="" > {{$subject_v->name}} <img src="/storage/{{$subject_v->image }}" style="max-width: 400px;" class="d-block w-100" alt="...">  </a>
    <br>
    <td> <a href="unhide/{{$subject_v->name}}" type="button"
        class="btn btn-outline-primary"> Unhide </a>
    </td>
@endforeach
</div>
@endsection
