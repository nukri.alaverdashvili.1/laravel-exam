@extends('layouts.app')

@section('content')
<div class="container">

<form action="create" method="post">
    @csrf   

    <div class="mb-3">
            <label for="subject">Choose a Subject:</label>
            <select name="subject">
            @foreach ($subject as $subject_v)
            <option value="{{$subject_v->id}}">{{$subject_v->name}}</option>
            @endforeach
            </select>
    </div>

    <div class="mb-3">
        <label class="form-label">Question</label>
        <input type="text" name="question" class="form-control">
    </div>
    <div class="mb-3">
        <label class="form-label">Answer</label>
        <input type="text" type="checkbox" name="answer" class="form-control">
        <input type="checkbox" name="correct" value="answer">
    </div>

    <div class="mb-3">
        <label class="form-label">Answer1</label>
        <input type="text" name="answer1" class="form-control">
        <input type="checkbox" name="correct" value="answer1">
    </div>

    <div class="mb-3">
        <label class="form-label">Answer2</label>
        <input type="text" name="answer2" class="form-control">
        <input type="checkbox" name="correct" value="answer2">
    </div>

    <div class="mb-3">
        <label class="form-label">Answer3</label>
        <input type="text" name="answer3" class="form-control">
        <input type="checkbox" name="correct" value="answer3">
    </div>

    <input type="submit" value="Submit">

</form>

</div>
@endsection