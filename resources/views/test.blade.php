@extends('layouts.app')

@section('content')
<div class="container">

    @foreach ($result as $testresult_v)

        <table class="table">
            <thead>
            <tr>
                <th scope="col">Question</th>
                <th scope="col">Answer</th>
            </tr>
            </thead>

            <tbody>
            <tr>
                @php $counter = 0; @endphp

                @foreach ($testresult_v->testanswer as $testanswer)
                @php if($testanswer->answer->correct){$counter++;} @endphp
                
                        <td>{{$testanswer->question->question}}</td>
                @if (Str::contains($testanswer->answer->answer, 'uploads'))
                    <td> <img style="max-width: 320px;" src="/storage/{{$testanswer->answer->answer}}" alt="">  </td>
                    <tr> </tr>
                @else
                        <td>{{$testanswer->question->question}}</td>
                    <tr> </tr>
                @endif
                @endforeach
                <th style="text-align: center" class="pt-5 pb-5"> Your Final Result Is {{$counter}} </th >

            </tr>

            </tbody>
        </table>
                    
    @endforeach



</div>
@endsection