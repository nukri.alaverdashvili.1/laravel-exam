@extends('layouts.app')

@section('content')

<div class="container">

 <h4> Exam </h4>   
 
    @foreach ($subject as $subject_info)

    @if ($subject_info->question->isEmpty())
            @include('home')
    @else
        <form action='{{$subject_info->name}}' method="post">
            @csrf
            @foreach ($subject_info->question as $question)
            @if (Str::contains($question->question, 'uploads'))
            <img style="max-width: 320px;" src="/storage/{{$question->question}}" alt=""><br>                            
            @else    
            <label style="font-size: 17px;"> <strong> {{$question->question}} </strong> </label> <br>
            @endif
                @foreach ($question->answer as $answer)
              
                @if (Str::contains($answer->answer, 'uploads'))
                    <input type="radio"  name="{{$question->id}}" value="{{$answer->id}}"> 
                    <img style="margin-bottom: 20px; max-width: 320px;" src="/storage/{{$answer->answer}}" alt=""><br>                            
                @else
                    <input type="radio"  name="{{$question->id}}" value="{{$answer->id}}">
                    <label for="html"> {{$answer->answer}}</label><br> 
                @endif
                @endforeach
                {{-- style="white-space: pre;" --}}
            @endforeach
            <input type="submit" value="Submit">

        </form>
    @endif
    
    @endforeach




</div>
@endsection