@extends('layouts.app')

@section('content')
    <div class="container">

    <div class="mt-5">
        <h3 class="font-monospace" style="text-decoration:none; color:gray;"> Start Exam </h3>
        
        @foreach ($subject as $subject_v)
            <a style="display: block;  text-decoration:none; color:gray;" href="exam/{{$subject_v->name}}" >{{$subject_v->name}} <br>
            <img style="max-width: 320px" src="storage/{{$subject_v->image}}"> </a>

             <a style="margin:20px 0" href="hide/{{$subject_v->name}}" type="button" class="btn btn-outline-primary"> Hide </a> 
        @endforeach

        <h3 class="font-monospace" style="text-decoration:none; color:gray;"> Show Result </h3>

        @foreach ($subject as $subject_v)
            <a style="display: inline-block;  text-decoration:none; color:gray;" href="show/{{$subject_v->name}}" >{{$subject_v->name}} <br>
            <img style="max-height: 240px;" src="storage/{{$subject_v->image}}"> </a>
        @endforeach


    </div>

    </div>
@endsection
