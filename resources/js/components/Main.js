import React, { useState, useEffect } from "react";
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import Home from './home/Home';
import Profile from './profile/Profile';
import Register from './home/Register';
import Login from './home/Login';
import Exam from './profile/Exam';
import Error from './profile/Error';
import Results from './profile/Results';
import Cookies from "js-cookie";
import Create from './profile/Create'

const Main = ()=> {
  const [login, setLogin] = useState(Cookies.get("token") ? Cookies.get("token") : null);
  const [subject, setSubject] = useState([]);
  const [loginData, setLoginData] = useState(Cookies.get("token"));
  useEffect(async ()=>{
    await axios.post('http://127.0.0.1:8000/api/profile',{loginData, proftype:'hide'}).then((res)=>
    {
        setSubject(res.data)

    }
    ).catch((error)=>{
        setError('Server Error 505')
    })
},[])
  return (
    <Router>
      <Routes>
        {!login ? <Route path='/' element={<Home/>}/> : <Route path='/' element={<Profile proftype ='hide'/>} />}
        {!login ? <Route path='/register' element={<Register/>} /> : <Route path='/register' element={<Profile proftype ='hide'/>} />}
        {!login ? <Route path='/login' element={<Login/>} />:<Route path='/login' element={<Profile proftype ='hide'/>} />}

        {login ?<Route path='/profile' element={<Profile proftype ='hide'/>} />: <Route path='/profile' element={<Home/>} />}
        {login && <Route path='/hidden' element={<Profile proftype='unhide' />} />}
        {login && <Route path='/createquestion' element={<Create />} />}

        {login && <Route path='/exam/informatic' element={<Exam subj='informatic'/>} />}
        {login && <Route path='/exam/geography' element={<Exam subj='geography'/>} />}
        {login && <Route path='/exam/history' element={<Exam subj='history'/>} />}
        {login && <Route path='/exam/Physik' element={<Exam subj='Physik'/>} />}
        {login && <Route path='/exam/Mathematik' element={<Exam subj='Mathematik'/>} />}

        {login && <Route path='/result/informatic' element={<Results  subj='informatic'/>} />}
        {login && <Route path='/result/geography' element={<Results subj='geography' />} />}
        {login && <Route path='/result/history' element={<Results subj='history' />} />}
        {login && <Route path='/result/Physik' element={<Results subj='Physik' />} />}
        {login && <Route path='/result/Mathematik' element={<Results subj='Mathematik' />} />}

        {/* {login && subject.map((sub)=>{
          return <Route path={'/exam/'+sub.name} element={<Exam subj={sub.name}/>} />
        })}
        
        {login && subject.map((sub)=>{
          return <Route path={'/result/'+sub.name} element={<Results subj={sub.name}/>} />
        })} */}



        <Route path='*' element={<Error/>}/>

      </Routes>
    </Router>
  );
}

ReactDOM.render(<Main />, document.getElementById('main'));
