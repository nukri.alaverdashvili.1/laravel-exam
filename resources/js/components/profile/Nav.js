import React from 'react'
import "bootstrap/dist/css/bootstrap.min.css";
import { Link } from "react-router-dom";
import * as RiIcons from 'react-icons/ri'
import * as AiIcons from 'react-icons/ai'
import * as BiIcons from 'react-icons/bi'
import * as GiIcons from 'react-icons/gi'
import Cookies from 'js-cookie'

function Nav() {
    const logout = ()=>{
        Cookies.remove("token");
        window.location ='/';
    }
    return (
        <div className='mt-5'>
           <ul className="nav flex-column">
            <li className="nav-item">
                <Link className="nav-link" style={{textDecoration:'none', color:"white"}} aria-current="page" to="/"><AiIcons.AiTwotoneHome className='mr-3' size={'45px'} /> Home</Link>
            </li>
            <li className="nav-item">
                <Link className="nav-link"  style={{textDecoration:'none', color:"white"}} to="/hidden"><BiIcons.BiHide className='mr-3' size={'45px'}/>Hide Subjects</Link>
            </li>
            <li className="nav-item">
                <Link className="nav-link"  style={{textDecoration:'none', color:"white"}} to="/createquestion"><GiIcons.GiNotebook className='mr-3' size={'45px'}/>Create Question</Link>
            </li>
            <li className="nav-item">
                <Link className="nav-link" onClick={logout}  style={{textDecoration:'none', color:"white"}} to='/'><RiIcons.RiLogoutCircleRLine className='mr-3' size={'45px'} /> Logout</Link>
            </li>
            </ul>
        </div>
    )
}

export default Nav
