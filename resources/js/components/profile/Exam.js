import React, { useState, useEffect, useReducer } from "react";
import { RadioGroup } from "react-rainbow-components";
import "bootstrap/dist/css/bootstrap.min.css";
import Nav from './Nav';
import Cookies from 'js-cookie'
import { useNavigate,  } from "react-router-dom";

function Exam({ subj }) {

    function reducer(state, action){

        switch (action.type){
            case action.type:
                return [...state, newTodos(action.type, action.payload.value)];
            default:
                break;
        }

        function newTodos(id, value)
        {
            if(values.length == 0)
            {
               return setValue([{id:id, value:value}])
            }else{
                const filter = values.filter((valFilter)=>{
                    if(valFilter && valFilter.id == id){
                        return valFilter;
                    }
                })
                if(filter.length ==0)
                {
                    return setValue([...values,{id:id, value:value}])
                }else{
                    return setValue(values.map((val)=>{
                        if(val && val.id == id){
                            return {id:id, value:value} 
                        }else{
                            return val;
                        }
                    }))
                }
            }

        }
    }

    const [values, setValue] = useState([]);
    const [question, setQuestion] = useState([]);
    const [loginData, setLoginData] = useState(Cookies.get("token"));
    const [state, dispatch] = useReducer(reducer, []);
    const [error, setError] = useState();
    const [test, setTest] = useState([]);
    const [method, setMethod] = useState(false);
    let history = useNavigate();
    let result = 0;

    useEffect(async () => {
        await axios
            .post("http://127.0.0.1:8000/api/question", { subj })
            .then((res) => {
                setQuestion(res.data);
            })
            .catch((error) => {
                setError(error.message);
            });
    }, []);

    async function handleSubmit(e)
    {
        e.preventDefault();

          const result = await axios
            .post("http://127.0.0.1:8000/api/savetest", { loginData, values, subj })
            .then((res) => {
                setTest(res.data);
                setMethod(true);
                // history({
                //     pathname:'/results',
                //     state: { detail: res.data },
                // })
            })
            .catch((error) => {
                setError('Server Error 505');
            });

    }

    function checkValue (id)
    {
        const find = values.find((val)=>{
        if(val.id == id)
        {
            return val.value
        }
        })
        if(find && find.value)
        {
            return find.value;
        }
    }

    return (
        <div className='container-fluid'>
        <div className="row" style={{height:'1500px'}}>
                <div className="col-2 bg-dark">
                    <Nav />
                </div>

                <div className="col-10 border-dark mt-5 mb-5">
                <div style={{textAlign:'center', justifyContent:"center", display:'grid'}} >
                {error && <div className="alert alert-danger mt-5" role="alert">
                    <h5>{error}</h5>
                </div>}
                </div>   
                {/* Exam Region */}
                    {!method ? 
                        <div className="container pt-5 pb-5" style={{borderWidth:'5px', borderStyle:'solid'}}>
                            <h1 style={{textAlign:'center'}}>Exam {subj}</h1>
                            <form onSubmit={handleSubmit}>
                            {question.map((quest) => {
                                return (
                                    <RadioGroup
                                        options={
                                            quest.answer &&
                                            quest.answer.map((answer) => {
                                                return {
                                                    value: answer.id.toString(),
                                                    label: answer.answer,
                                                };
                                            })
                                        }
                                        onChange={(e) => dispatch({type: quest.id, payload:{value:e.target.value}})}
                                        value={checkValue(quest.id)}
                                        label={quest.question}
                                    />
                                );
                            })}
                            <button className="btn btn-dark mt-5"> Submit</button>
                        </form>
                        </div>
                        // Answer Region
                        :
                        <div className="col-10 mt-5 mb-5">
                            <div className="container pt-5 pb-5" style={{borderWidth:'5px', borderStyle:'solid', borderColor:'gray'}}>
                            <table className="table">
                            <thead>
                            <tr>
                                <th scope="col">Question</th>
                                <th scope="col">Answer</th>
                            </tr>
                            </thead>
                                {test.testanswer.map((testanswer)=>{
                                    if(testanswer.answer.correct ==1)
                                    {
                                        result++;
                                    }
                                    return(
                                        <tbody>
                                        <tr>
                                            <td>{testanswer.question.question}</td>
                                            <td>{testanswer.answer.answer}</td>
                                        </tr>
                                        </tbody>
                                    )
                                })}
                            </table>
                            <h5 style={{textAlign:'center'}} className="pt-5 pb-5"> Your Final Result Is {result} </h5 >
                            </div>
                    </div>
                    }
                </div>
            </div>
        </div>
    );
}

export default Exam;
