import React, { useState, useEffect } from "react";
import { RadioGroup } from "react-rainbow-components";
import "bootstrap/dist/css/bootstrap.min.css";
import Nav from './Nav';
import Cookies from 'js-cookie'


function Results({subj}) {
    const [loginData, setLoginData] = useState(Cookies.get("token"));
    const[error, setError]= useState('');
    const [tests,setTests] = useState([]);    
    let result = 0;

    useEffect(async () => {
        const result = await axios
            .post("http://127.0.0.1:8000/api/testresults", { subj, loginData })
            .then((res) => {
                setTests(res.data);
            })
            .catch((error) => {
                setError(error.message);
            });
    }, []);
    return (
        <div className='container-fluid'>
        <div className="row" style={{height:'1500px'}}>
                <div className="col-2 bg-dark">
                    <Nav />
                </div>

                <div className="col-10 mt-5 mb-5">
                <div style={{textAlign:'center', justifyContent:"center", display:'grid'}} >
                {error && <div className="alert alert-danger mt-5" role="alert">
                    <h5>{error}</h5>
                </div>}
                </div>   
                    {tests.map((test)=>{
                        result = 0;
                        return(
                            <div className="container pt-5 pb-5" style={{borderWidth:'5px', borderStyle:'solid', borderColor:'gray'}}>
                            <table className="table">
                            <thead>
                            <tr>
                                <th scope="col">Question</th>
                                <th scope="col">Answer</th>
                            </tr>
                            </thead>
                                {test.testanswer.map((testanswer)=>{
                                    if(testanswer.answer.correct ==1)
                                    {
                                        result++;
                                    }
                                    return(
                                        <tbody>
                                        <tr>
                                            <td>{testanswer.question.question}</td>
                                            <td>{testanswer.answer.answer}</td>
                                        </tr>
                                        </tbody>
                                    )
                                })}
                            </table>
                            <h5 style={{textAlign:'center'}} className="pt-5 pb-5"> Your Final Result Is  {result} </h5 >
                            <h5 style={{textAlign:'center'}} className="pt-2 pb-2">  Test Create Date is  {test.created_at.substring(0, 10)} </h5 >

                            </div>
                            )
                        })}
                </div>
        </div>
        </div>
    )
}

export default Results
