import React, {useEffect, useState} from 'react'
// import Fundamentals from '../images/showcase.svg';
import Cookies from 'js-cookie'
import "bootstrap/dist/css/bootstrap.min.css";
import { Link } from "react-router-dom";
import Nav from './Nav';

function Profile({proftype}) {
    const [subject, setSubject] = useState([]);
    const [loginData, setLoginData] = useState(Cookies.get("token"));
    const [error, setError] = useState(false);
    const [message, setMessage] = useState(false);
    useEffect(async ()=>{
        await axios.post('http://127.0.0.1:8000/api/profile',{loginData, proftype}).then((res)=>
        {
            if (res.data.length == 0)
            {
                setMessage(proftype =='unhide' ? 'You dont have any hidden subjects' : 'All Subjects is Hidden, You can go Page hide Subjects and Unhide it')
            }else{setMessage(false)}
            setSubject(res.data)

        }
        ).catch((error)=>{
            setError('Server Error 505')
        })
    },[proftype])

    const hide = async (id)=>{
        await axios.post(proftype === 'hide' ? 'http://127.0.0.1:8000/api/hide': 'http://127.0.0.1:8000/api/unhide',{id, loginData, proftype}).then((res)=>{
            if(res.data.correct)
            {
                setSubject(subject.filter((sub)=>{
                    if(sub.id != id){
                        return sub;
                    }
                }))
            }
        }).catch((error)=>{
            setError('Server Error 505')
        })
    }

    return (
        <div className='container-fluid'>
        <div className="row" style={{height:'1500px'}}>
          <div className="col-2 bg-dark">
            <Nav />
          </div>
        
        <div className="col-10">
        <div style={{textAlign:'center', justifyContent:"center", display:'grid'}} >
            {error && <div className="alert alert-danger mt-5" role="alert">
                <h5>{error}</h5>
            </div>}
         </div>   
         <div style={{textAlign:'center', justifyContent:"center", display:'grid'}} >
            {message && <div className="alert alert-info mt-5" role="alert">
                <h5>{message}</h5>
            </div>}
         </div>   
            <div className='row'>
            {subject.map((sub) => {
            return( 
            <div key={sub.id} className="d-inline mt-5 mb-5" style={{width:'520px', marginLeft:'80px'}}>
                    <img className="img-fluid" style={{height:'450px'}} src={('/storage/'+sub.image)}></img>
                    <div >
                        <h5 className="card-title">{sub.name}</h5>
                        <p className='card-text' >This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                        <p> <small className="text-muted">Min Answer is: {sub.min}</small></p>
                        { proftype ==='hide' ? <Link to={'/exam/'+sub.name} className='btn btn-dark'>Write Exam</Link>: null}
                        { proftype ==='hide' ? <Link to={'/result/'+sub.name} className='btn btn-dark ml-3'>See Results</Link>: null}
                        <button className='btn btn-dark ml-3' onClick={()=>hide(sub.id)}> {proftype=='hide' ? `hide` : `unhide`} </button>
                    </div>
            </div>
            )
        })}
        </div>
        
        </div>
        
        </div>
        </div>

    )
}

export default Profile
