import React from 'react'
import Nav from './Nav'

function Error() {
    return (
        <div className='container-fluid'>
        <div className="row" style={{height:'1000px'}}>
          <div className="col-2 bg-dark">
            <Nav />
          </div>
        
        <div className="col-10 d-sm-flex align-items-center justify-content-between text-center">
            <h1 className='ml-auto mr-auto alert alert-danger'>Error 404 Not Found</h1>
        </div>
        </div>
        </div>
    )
}

export default Error
