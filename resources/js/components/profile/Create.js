import React, {useState, useEffect} from 'react'
import Nav from './Nav'
import { Input, Select } from "react-rainbow-components";
import Cookies from 'js-cookie'
import { useNavigate,  } from "react-router-dom";


function Create() {
  const [subject, setSubject] = useState([]);
  const[sub, setSub] = useState('');
  const [question, setQuestion] = useState('');
  const [answer1, setAnswer1] = useState([]);
  const [answer2, setAnswer2] = useState([]);
  const [answer3, setAnswer3] = useState([]);
  const [answer4, setAnswer4] = useState([]);
  const [error, setError] = useState(false);
  const [loginData, setLoginData] = useState(Cookies.get("token"));
  const [correct, setCorrect] = useState([]);
  let history = useNavigate();
  
    const inputStyles = {
        width: 500,
      };

      useEffect(async ()=>{
        await axios.post('http://127.0.0.1:8000/api/profile',{loginData}).then((res)=>
        {
            setSubject(res.data);
            setSub(res.data[0].id);
        }
        ).catch((error)=>{
            setError('Server Error 505')
        })
    },[])

   async function onSubmit(e)
   {    
       e.preventDefault();

       await axios.post('http://127.0.0.1:8000/api/createquestion',{sub, question, answer1, answer2, answer3, answer4, correct}).then((res)=>
       {
                history('/');
       }
       ).catch((error)=>{
           setError('Server Error 505')
       })
          }

    return (
        <div className='container-fluid'>
        <div className="row" style={{height:'1000px'}}>
          <div className="col-2 bg-dark">
            <Nav />
          </div>
        
          <div className='col-10 '>
            <div className='container  justify-content-center align-content-center mt-5 ml-auto mr-auto'>
        {error && (
          <div className="alert alert-danger" role="alert">
            <h5>{error}</h5>
          </div>
        )}

            <form onSubmit={onSubmit}>
            <Select
            label="Status"
            style={inputStyles}
            options={subject.map((sub) => {
              return { value:parseInt(sub.id), label: sub.name };
            })}
            onChange={(e)=>setSub(parseInt(e.target.value))}
            className="rainbow-m-vertical_x-large rainbow-p-horizontal_medium rainbow-m_auto"
          />
            <Input
                label="Question"
                placeholder="Question"
                type="text"
                className="rainbow-p-around_medium"
                style={inputStyles}
                value={question}
                onChange={(e)=>setQuestion(e.target.value)}
            />
            <Input
                label="Answer1"
                placeholder="Answer1"
                type="text"
                className="rainbow-p-around_medium"
                style={inputStyles}
                value={answer1}
                onChange={(e)=>setAnswer1(e.target.value)}
            />
            <input type="checkbox" name="correct" defaultValue={answer1} onChange={(e)=>setCorrect([...correct, e.target.defaultValue])} />
            <Input
                label="Answer2"
                placeholder="Answer2"
                type="text"
                className="rainbow-p-around_medium"
                style={inputStyles}
                value={answer2}
                onChange={(e)=>setAnswer2(e.target.value)}
            />
            <input type="checkbox" name="correct" defaultValue={answer2} onChange={(e)=>setCorrect([...correct, e.target.defaultValue])}/>

            <Input
                label="Answer3"
                placeholder="Answer3"
                type="text"
                className="rainbow-p-around_medium"
                style={inputStyles}
                value={answer3}
                onChange={(e)=>setAnswer3(e.target.value)}
            />
            <input type="checkbox" name="correct" defaultValue={answer3} onChange={(e)=>setCorrect([...correct, e.target.defaultValue])} />

            <Input
                label="Answer4"
                placeholder="Answer4"
                type="text"
                className="rainbow-p-around_medium"
                style={inputStyles}
                value={answer4}
                onChange={(e)=>setAnswer4(e.target.value)}
            />
            <input type="checkbox" name="correct" defaultValue={answer4} onChange={(e)=>setCorrect([...correct, e.target.defaultValue])} />

            <button style={{marginLeft:'150px'}} className='btn btn-success mt-3'>Submit</button>
            </form>
        </div>
         </div>
        </div>
        </div>
    )
}

export default Create
