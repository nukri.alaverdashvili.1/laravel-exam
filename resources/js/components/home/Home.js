import {React} from 'react'
import ReactDOM from 'react-dom';
import { Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css"
import Image from '../images/undraw_exams_g-4-ow.svg'
import Fundamentals from '../images/show3.jpg'
import { FcUp } from "react-icons/fc";
import {BsFacebook,BsTwitter, BsInstagram} from 'react-icons/bs'

function Home() {
    return (
        <>
        {/* Navbar */}
        <nav className="navbar navbar-expand-lg bg-dark navbar-dark py-3 fixed-top" >    {/* ნავიგაციაში როდესაც lg იქნება ეკრანის ზომა მაშინ გამოჩნდეს მხოლოდ ეს მენიუ. რომელიც ბექგრაუნდი ექნება დარქი */}
        <div className="container">   {/* a ტეგი იქნება კონტეინერში რომელიც გვერდებიდან თანაბრად იქნება დაშორებული */}
          <a href="#" className="navbar-brand"> ExamCenter</a>
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navmenutoshow"> <span className="navbar-toggler-icon"></span> </button>{/* დამალული იქნება ეს ბათნი, აიდის მიხედვით ამ ბათნზე დაწოლისას გამოჩნდება მენიუ */}
    
          <div className="collapse navbar-collapse" id="navmenutoshow">
            <ul className="navbar-nav ms-auto"> {/* ms-auto ნიშნავს რომ მარგჯინი სტარტიდან ანუ დაშორება მენიუსგან ავტო იქნება და ბოლოში გადავა მენიუ */}
              <li className="nav-item">
                <a href="#about" className="nav-link">About</a>
              </li>
              <li className="nav-item">
                <a href="#team" className="nav-link">Team</a>
              </li>
              <li className="nav-item">
                <Link to="/register" className="nav-link">Registration</Link>
              </li>
              <li className="nav-item">
                <Link to="/login" className="nav-link">Login</Link>
              </li>
            </ul>
          </div>
        </div>
          </nav> 
        {/* ShowCase */}
        <section className="bg-dark p-5 text-light text-center text-sm-start"> {/* small მდე ტექსტი იქნება სტარტზე ხოლო სმოლის სემთხვევაში გაცენტრდება ტექსტი */}
          <div className="container pt-5">
            <div className="d-sm-flex align-items-center justify-content-between">  {/* ამ დივში არსებულ ელემენტებს თანაბრად გაანაწილებს small ეკრანამდე, ხოლო სმოლზე ფლექსი აღარ იქნება */}
              <div >
                <h1>Ihr ExamCenter <span className="text-warning"> für Verschiedene Fächer </span> </h1> {/* text-warning გააყვითლებს ტექსტს */}
                <p className="lead">
                Du kannst In verschiedene fächer prüfung schreiben  
                </p>
              </div>
              <img className="img-fluid w-50 d-none d-sm-block" src={Image}></img> {/* w-50 იმიჯის ზომას გაანახევრებს d-none ნიშნავს რომ არ გამოჩნდება ფოტო d-sm-block ნიშნავს რომ ფოტო გამოჩნდება სანამ ეკრანი სმოლი იქნება */}
            </div>
          </div>
        </section>
    
        {/* LearnSection */}
    
        <section id="about" className="p-5">
          <div className="container">
            <div className="row align-items-center justify-content-between">
                <div className="col-md">
                  <img className="img-fluid" src={Fundamentals}></img>
                </div>
    
                <div className="col-md">
                <h2>About: </h2>
                  <h5>ExamCenter GmbH </h5>
                  <h5>Drackendorfer str 14 </h5>
                  <h5>07747 Jena</h5>
                  <h5> Telefon: 03641-274110 </h5>
                  <h5>E-Mail: examinfo@gmail.com</h5>
                  <a href="#" className="btn btn-light mt-3">
                  </a>
                </div>
            </div>
          </div>
        </section>
    
            {/* ReactSection */}

        {/*  */}
    
        <section id="team" className="p-5 bg-dark" >
          <div className="container">
            <h2 className="text-center text-white">Our Team</h2>

            <div className="row g-4">
                <div className="col-md-6 col-lg-3"> 
                  <div className="card bg-light">
                    <div className="card-body text-center">
                        <img className="rounded-circle mb-3" style={{width:'130px'}} src="https://randomuser.me/api/portraits/men/1.jpg"></img>
                        <h3 className="card-title mb-3">James</h3>
                        <p className="card-text">
                        Placeholder content for this accordion, which is intended to demonstrat
                        </p>
                        <a href="#"><i className="bi bi-twitter text-dark mx-1"></i></a>
                        <a href="#"><i className="bi bi-facebook text-dark mx-1"></i></a>
                        <a href="#"><i className="bi bi-linkedin text-dark mx-1"></i></a>
                        <a href="#"><i className="bi bi-instagram text-dark mx-1"></i></a>
                    </div>   
                  </div>
                </div>
    
                <div className="col-md-6 col-lg-3"> 
                  <div className="card bg-light">
                    <div className="card-body text-center">
                        <img className="rounded-circle mb-3" src="https://randomuser.me/api/portraits/men/15.jpg"></img>
                        <h3 className="card-title mb-3">Adam Le</h3>
                        <p className="card-text">
                        Placeholder content for this accordion, which is intended to demonstrat
                        </p>
                        <a href="#"><i className="bi bi-twitter text-dark mx-1"></i></a>
                        <a href="#"><i className="bi bi-facebook text-dark mx-1"></i></a>
                        <a href="#"><i className="bi bi-linkedin text-dark mx-1"></i></a>
                        <a href="#"><i className="bi bi-instagram text-dark mx-1"></i></a>
                    </div>   
                  </div>
                </div>
                
                <div className="col-md-6 col-lg-3"> 
                  <div className="card bg-light">
                    <div className="card-body text-center">
                        <img className="rounded-circle mb-3" src="https://randomuser.me/api/portraits/women/11.jpg"></img>
                        <h3 className="card-title mb-3">Anna fran</h3>
                        <p className="card-text">
                        Placeholder content for this accordion, which is intended to demonstrat
                        </p>
                        <a href="#"><i className="bi bi-twitter text-dark mx-1"></i></a>
                        <a href="#"><i className="bi bi-facebook text-dark mx-1"></i></a>
                        <a href="#"><i className="bi bi-linkedin text-dark mx-1"></i></a>
                        <a href="#"><i className="bi bi-instagram text-dark mx-1"></i></a>
                    </div>   
                  </div>
                </div>
    
                <div className="col-md-6 col-lg-3"> 
                  <div className="card bg-light">
                    <div className="card-body text-center">
                        <img className="rounded-circle mb-3" src="https://randomuser.me/api/portraits/men/5.jpg"></img>
                        <h3 className="card-title mb-3">Alex Doe</h3>
                        <p className="card-text">
                        Placeholder content for this accordion, which is intended to demonstrat
                        </p>
                        <a href="#"><i className="bi bi-twitter text-dark mx-1"></i></a>
                        <a href="#"><i className="bi bi-facebook text-dark mx-1"></i></a>
                        <a href="#"><i className="bi bi-linkedin text-dark mx-1"></i></a>
                        <a href="#"><i className="bi bi-instagram text-dark mx-1"></i></a>
                    </div>   
                  </div>
                </div>
            </div>
          </div>
        </section>
    
        {/* Footer */}
        <footer className="p-5 bg-light text-dark text-center">
        <div className="container">
              <a href='https://www.facebook.com/'><BsFacebook className='mr-3' size={'45px'}/></a>
              <a href='https://www.twitter.com/'><BsTwitter className='mr-3' size={'45px'}/></a>
              <a href='https://www.instagram.com/'><BsInstagram className='mr-3' size={'45px'}/></a>
               Copyright && Copy ExamCenter GmbH
        </div>
        </footer>
        </>
    )
}

export default Home;

