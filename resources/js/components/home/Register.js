import React, {useState, useEffect} from 'react'
import { Input } from 'react-rainbow-components';
import axios from 'axios'
import Registerimg from '../images/Register.svg';
import { useNavigate, Link } from "react-router-dom";
import Validation from './Validate'

const inputStyles = {
    width: 500,
    marginTop:25,
};

function Register() {
    const [name, setName]=useState('')
    const [password, setPassword]=useState('')
    const [password2, setPassword2]=useState('')
    const [email, setEmail]=useState('')
    const [error, setError]=useState(false)
    const [success, setSuccess] = useState(false)

    let history = useNavigate();

    async function signUp(e) //ფუნქცია გამოიძახება რეგისტრაციის ღილაკს დააჭერენ ხელს
    {
        e.preventDefault();
        let item={name,password,email, password2} //ყველა ცვლადს რომელიც ზემოთ აღვწერეთ ვინახავთ item ში

        let check = Validation(item) //ვუკეთებთ ითემებს ვალიდაციას, რომელიც ცალკე ფუნქციაა
        if(!check) //თუ ვალიდაციის შემდეგ ფალსი დაბრუნდება
        {
            await axios.post('http://127.0.0.1:8000/api/register',{name, password, email}).then((res)=>{
                if(res.data.error)
                {
                    setError({msg:res.data.error});
                }else{
                    setError('');
                    setSuccess('Successfuly Registrierd Please Logged In')
                    window.setTimeout(function(){
                        history('/login')
                    },1800)
                }
            }).catch((e)=>{
                setError({msg:'Server Error 505'})
            })
        }else{
            setError(check) //ერორის შემთხვევაში 
        }
    }
    return (
        <div className='' style={{height:'1500px', backgroundImage:`url(https://cdn.wallpapersafari.com/73/82/0zuld2.jpg)`, backgroundSize:'cover', backgroundRepeat:'no-repeat'}}>
        <div className='container'>
        <div style={{textAlign:'center', justifyContent:"center", display:'grid'}} >
           <Link to='/'><img className="img-fluid mb-5 w-50" src={Registerimg}></img> </Link>   
        </div>
       <div style={{textAlign:'center', justifyContent:"center", display:'grid'}} >
       {error &&
       <>
       {error.name && <div className="alert alert-danger" role="alert"> {/* თუ ნეიმის ერორია მაშგ შემთხვევაში გამოიტანს მხოლოდ ამ შეტყობინებას */}
           <h5>{error.name}</h5>
       </div>
        }
        {error.password && 
       <div className="alert alert-danger" role="alert">
           <h5>{error.password}</h5>
       </div>
        }
         {error.msg && 
       <div className="alert alert-danger" role="alert">
           <h5>{error.msg}</h5>
       </div>
        }
        </>
        }

        {!error && success && <div className="alert alert-success" role="alert">{/* თუ ერორები არ არის და საქსესი თრუ არის მაგ შემთხვევაში გამოიტანს საქსესის მესიჯს */}
                <h5> {success} </h5>
        </div>}

        <form onSubmit={signUp} >
            <Input
                placeholder="Name"
                type="text"
                className="rainbow-p-around_medium"
                style={inputStyles}
                value={name}  //ამ ინფუთის მნიშვნელობა არის ზემოთ აღნიშნული ნეიმი თავიდან ცარიელი სტრინგი
                onChange={(e)=>setName(e.target.value)} // ინფუთში რამის ჩაწერის შემდეგ ნეიმი შეიცვლება და გახდება ის მნიშვნელობა რასაც ინფუთში ჩავწერთ
                />

            <Input
                placeholder="**********"
                type="password"
                className="rainbow-p-around_medium"
                style={inputStyles}
                value={password} 
                onChange={(e)=>setPassword(e.target.value)}
            />
            <Input
                placeholder="Confirm Password"
                type="password"
                className="rainbow-p-around_medium"
                style={inputStyles}
                value={password2} 
                onChange={(e)=>setPassword2(e.target.value)}
            />
            <Input
                placeholder="inputEmail@gmail.com"
                type="email"
                className="rainbow-p-around_medium"
                style={inputStyles}
                value={email} 
                onChange={(e)=>setEmail(e.target.value)}
            />
            <input type="submit" className="btn btn-dark align-content-center mt-5"/>    
            </form>
       </div>
        </div>
        </div>
    )
}

export default Register
