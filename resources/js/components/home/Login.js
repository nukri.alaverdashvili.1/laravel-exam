import React, {useState} from 'react'
import { Input } from 'react-rainbow-components';
import axios from 'axios'
import Loginimg from '../images/Login.svg';
import Cookies from 'js-cookie'
import { useNavigate, Link } from "react-router-dom";


const inputStyles = {
    width: 500,
    marginTop:25,
};

function Login() {
    const [email, setEmail]=useState('')
    const [password, setPassword]=useState('')
    const [error, setError]=useState(false)
    let history = useNavigate();

    async function signUp(e)
    {
        e.preventDefault();
        const result = await axios.post('http://127.0.0.1:8000/api/login',{password, email}).then((res)=>{
            if(res.data.error)
            {
                setError(res.data.error);
            }else{
                Cookies.set('token', res.data.id) 
                window.location = '/profile'
            }
        }).catch((err)=>{
            setError('Server Error');
        });
    }
    return (
        <div className='' style={{height:'1500px', backgroundImage:`url(https://images.unsplash.com/photo-1568035012504-963fcc93852c?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1yZWxhdGVkfDE0fHx8ZW58MHx8fHw%3D&w=1000&q=80)`, backgroundSize:'cover', backgroundRepeat:'no-repeat'}}>
        <div className='container'>
        <div style={{textAlign:'center', justifyContent:"center", display:'grid'}} >
         <Link to='/'> <img className="img-fluid mb-5 w-50" src={Loginimg}></img> </Link>
        </div>
        <div style={{textAlign:'center', justifyContent:"center", display:'grid'}} >
            {error && <div className="alert alert-danger" role="alert">
           <h5>{error}</h5>
       </div>
        }
        <form onSubmit={signUp} >
            <Input
                placeholder="Input Email"
                type="email"
                className="rainbow-p-around_medium"
                style={inputStyles}
                value={email} 
                onChange={(e)=>setEmail(e.target.value)}
                />

            <Input
                placeholder="**********"
                type="password"
                className="rainbow-p-around_medium"
                style={inputStyles}
                value={password} 
                onChange={(e)=>setPassword(e.target.value)}
            />
            <input type="submit" className="btn btn-dark align-content-center mt-5"/>    
            </form>
          </div>
        </div>
        </div>
    )
}

export default Login
