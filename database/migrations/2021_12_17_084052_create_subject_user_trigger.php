<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateSubjectUserTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
        CREATE TRIGGER create_user AFTER INSERT ON `users`
        FOR EACH ROW
        BEGIN
                INSERT INTO subject_user(subject_id, user_id, pass_exam, hide)
                VALUES
                (1,NEW.id, 0,0),
                (2,NEW.id, 0,0),
                (3,NEW.id, 0,0), 
                (4,NEW.id, 0,0), 
                (5,NEW.id, 0,0); 
        END;
        ');
    }
  
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER `delete_user`');
    }
}
