<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HistoryAnswerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('answer')->insert(array(
            0=>array(
                'question_id' => '11',
                'answer' => 'philip Adams',
                'correct' => '0'
            ),
            1=>array(
                'question_id' => '11',
                'answer' => 'Neil Armstrong',
                'correct' => '1'
            ),
            2=>array(
                'question_id' => '11',
                'answer' => 'John snow',
                'correct' => '0'
            ),
            3=>array(
                'question_id' => '11',
                'answer' => 'James Franco',
                'correct' => '0'
            ),
            4=>array(
                'question_id' => '12',
                'answer' => 'Abraham Lincoln',
                'correct' => '0'
            ),
            5=>array(
                'question_id' => '12',
                'answer' => 'John Adams',
                'correct' => '0'
            ),
            6=>array(
                'question_id' => '12',
                'answer' => 'George Washington',
                'correct' => '1'
            ),
            7=>array(
                'question_id' => '12',
                'answer' => 'barak obama',
                'correct' => '0'
            ),
            8=>array(
                'question_id' => '13',
                'answer' => 'Declaration of war',
                'correct' => '0'
            ),
            9=>array(
                'question_id' => '13',
                'answer' => 'Declaration of Mosaic',
                'correct' => '0'
            ),
            10=>array(
                'question_id' => '13',
                'answer' => 'Declaration of freedom',
                'correct' => '0'
            ),
            11=>array(
                'question_id' => '13',
                'answer' => 'Declaration of Independence',
                'correct' => '1'
            ),
            12=>array(
                'question_id' => '14',
                'answer' => 'John Canady',
                'correct' => '0'
            ),
            13=>array(
                'question_id' => '14',
                'answer' => 'John Adams',
                'correct' => '1'
            ),
            14=>array(
                'question_id' => '14',
                'answer' => 'Samuel Wilson',
                'correct' => '0'
            ),
            15=>array(
                'question_id' => '14',
                'answer' => 'John Wilkes Booth',
                'correct' => '0'
            ),
            16=>array(
                'question_id' => '15',
                'answer' => '1865',
                'correct' => '0'
            ),
            17=>array(
                'question_id' => '15',
                'answer' => '1862',
                'correct' => '1'
            ),
            18=>array(
                'question_id' => '15',
                'answer' => '1789',
                'correct' => '0'
            ),
            19=>array(
                'question_id' => '15',
                'answer' => '1901',
                'correct' => '0'
            ),
        ));
    }
}
