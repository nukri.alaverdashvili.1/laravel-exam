<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class QuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('question')->delete();
        DB::table('question')->insert(array(
            0=>array(
                'id'=>1,
                'subject_id' => '1',
                'question' => ' Which one is the first search engine in internet',
            ),
            1=>array(
                'id'=>2,
                'subject_id' => '1',
                'question' => 'Number of bit used by the IPv6 address',
            ),
            2=>array(
                'id'=>3,
                'subject_id' => '1',
                'question' => 'Which one is the first web browser invented in 1990',
            ),
            3=>array(
                'id'=>4,
                'subject_id' => '1',
                'question' => 'Which of the following programming language is used to create programs like applets?',
            ),
            4=>array(
                'id'=>5,
                'subject_id' => '1',
                'question' => 'First computer virus is known as',
            ),
        ));
        DB::table('question')->insert(array(
            0=>array(
                'id'=>6,
                'subject_id' => '2',
                'question' => 'What is the name of the tallest mountain in the world? ',
            ),
            1=>array(
                'id'=>7,
                'subject_id' => '2',
                'question' => 'Which country has the largest population in the world? ',
            ),
            2=>array(
                'id'=>8,
                'subject_id' => '2',
                'question' => 'What American city is the Golden Gate Bridge located in?',
            ),
            3=>array(
                'id'=>9,
                'subject_id' => '2',
                'question' => 'What is the capital of Canada?',
            ),
            4=>array(
                'id'=>10,
                'subject_id' => '2',
                'question' => 'What is the name of the largest ocean in the world?',
            ),
        ));

        DB::table('question')->insert(array(
            0=>array(
                'id'=>11,
                'subject_id' => '3',
                'question' => ' Who was the first man on the moon?',
            ),
            1=>array(
                'id'=>12,
                'subject_id' => '3',
                'question' => 'Who was the first president of the United States?',
            ),
            2=>array(
                'id'=>13,
                'subject_id' => '3',
                'question' => 'What important document was signed in 1776?',
            ),
            3=>array(
                'id'=>14,
                'subject_id' => '3',
                'question' => 'Who was the first president to live in the White House?',
            ),
            4=>array(
                'id'=>15,
                'subject_id' => '3',
                'question' => 'When was the first American dollar printed?',
            ),
        ));
    }
}
