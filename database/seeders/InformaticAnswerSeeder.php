<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InformaticAnswerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('answer')->insert(array(
            0=>array(
                'question_id' => '1',
                'answer' => 'Archie',
                'correct' => '1'
            ),
            1=>array(
                'question_id' => '1',
                'answer' => 'Google',
                'correct' => '0'
            ),
            2=>array(
                'question_id' => '1',
                'answer' => 'Altavista',
                'correct' => '0'
            ),
            3=>array(
                'question_id' => '1',
                'answer' => 'WAIS',
                'correct' => '0'
            ),
            4=>array(
                'question_id' => '2',
                'answer' => '32 bit',
                'correct' => '0'
            ),
            5=>array(
                'question_id' => '2',
                'answer' => '64 bit',
                'correct' => '0'
            ),
            6=>array(
                'question_id' => '2',
                'answer' => '128 bit',
                'correct' => '1'
            ),
            7=>array(
                'question_id' => '2',
                'answer' => '256',
                'correct' => '0'
            ),
            8=>array(
                'question_id' => '3',
                'answer' => 'Internet Explorer',
                'correct' => '0'
            ),
            9=>array(
                'question_id' => '3',
                'answer' => 'Mosaic',
                'correct' => '0'
            ),
            10=>array(
                'question_id' => '3',
                'answer' => 'Mozilla',
                'correct' => '0'
            ),
            11=>array(
                'question_id' => '3',
                'answer' => 'Nexus',
                'correct' => '1'
            ),
            12=>array(
                'question_id' => '4',
                'answer' => 'COBOL',
                'correct' => '0'
            ),
            13=>array(
                'question_id' => '4',
                'answer' => 'CLanguage',
                'correct' => '0'
            ),
            14=>array(
                'question_id' => '4',
                'answer' => 'Java',
                'correct' => '1'
            ),
            15=>array(
                'question_id' => '4',
                'answer' => 'BASIC',
                'correct' => '0'
            ),
            16=>array(
                'question_id' => '5',
                'answer' => 'Rabbit',
                'correct' => '0'
            ),
            17=>array(
                'question_id' => '5',
                'answer' => 'Creeper Virus',
                'correct' => '1'
            ),
            18=>array(
                'question_id' => '5',
                'answer' => 'Elk Cloner',
                'correct' => '0'
            ),
            19=>array(
                'question_id' => '5',
                'answer' => 'SCA Virus',
                'correct' => '0'
            ),
        ));
    }
}
