<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GeographyAnswerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('answer')->insert(array(
            0=>array(
                'question_id' => '6',
                'answer' => 'Mount McKinley ',
                'correct' => '0'
            ),
            1=>array(
                'question_id' => '6',
                'answer' => 'Mount Logan',
                'correct' => '0'
            ),
            2=>array(
                'question_id' => '6',
                'answer' => 'Everest',
                'correct' => '1'
            ),
            3=>array(
                'question_id' => '6',
                'answer' => 'Mount Ushba',
                'correct' => '0'
            ),
            4=>array(
                'question_id' => '7',
                'answer' => 'Russia',
                'correct' => '0'
            ),
            5=>array(
                'question_id' => '7',
                'answer' => 'India',
                'correct' => '0'
            ),
            6=>array(
                'question_id' => '7',
                'answer' => 'Canada',
                'correct' => '0'
            ),
            7=>array(
                'question_id' => '7',
                'answer' => 'China',
                'correct' => '1'
            ),
            8=>array(
                'question_id' => '8',
                'answer' => ' San Francisco',
                'correct' => '1'
            ),
            9=>array(
                'question_id' => '8',
                'answer' => 'New York',
                'correct' => '0'
            ),
            10=>array(
                'question_id' => '8',
                'answer' => 'Alaska',
                'correct' => '0'
            ),
            11=>array(
                'question_id' => '8',
                'answer' => 'California',
                'correct' => '0'
            ),
            12=>array(
                'question_id' => '9',
                'answer' => 'Vancouver',
                'correct' => '0'
            ),
            13=>array(
                'question_id' => '9',
                'answer' => 'Ottawa',
                'correct' => '1'
            ),
            14=>array(
                'question_id' => '9',
                'answer' => 'Montreal',
                'correct' => '0'
            ),
            15=>array(
                'question_id' => '9',
                'answer' => 'Toronto',
                'correct' => '0'
            ),
            16=>array(
                'question_id' => '10',
                'answer' => 'Arctic',
                'correct' => '0'
            ),
            17=>array(
                'question_id' => '10',
                'answer' => 'The Pacific Ocean ',
                'correct' => '1'
            ),
            18=>array(
                'question_id' => '10',
                'answer' => 'Atlantic',
                'correct' => '0'
            ),
            19=>array(
                'question_id' => '10',
                'answer' => 'Indian',
                'correct' => '0'
            ),
        ));
    }
}
