<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subject')->delete();
        DB::table('subject')->insert(array(
            0=>array(
                'id'=>1,
                'name' => 'Informatic',
                'image' => 'uploads/it.jpg',
                'min' => 5,
            ),
            1=>array(
                'id'=>2,
                'name' => 'Geography',
                'image' => 'uploads/geography.PNG',
                'min' => 5,
            ),
            2=>array(
                'id'=>3,
                'name' => 'History',
                'image' => 'uploads/History.jpg',
                'min' => 5,
            ),
            3=>array(
                'id'=>4,
                'name' => 'Mathematik',
                'image' => 'uploads/mathe.jpg',
                'min' => 5,
            ),
            4=>array(
                'id'=>5,
                'name' => 'Physik',
                'image' => 'uploads/physik.jpg',
                'min' => 5,
            ),
        ));
    }
}
