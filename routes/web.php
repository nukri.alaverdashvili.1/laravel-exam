<?php

use App\Models\Subject;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//  try{
// 	$subjects = Subject::select(['id', 'name'])->get();
// 	foreach($subjects as $subject_k =>$subject_v)
// 	{
// 	    Route::get('exam/'.$subject_v->name, 'ExamController@exam')->defaults('subjectname', $subject_v->name);
// 	    Route::post('exam/'.$subject_v->name, 'ExamController@answer')->defaults('subjectname', $subject_v->name);
// 	    Route::get('hide/'.$subject_v->name, 'ExamController@hideunhide')->defaults('posiblesubject', $subject_v)->defaults('hideunhide', 1);
// 	    Route::get('unhide/'.$subject_v->name, 'ExamController@hideunhide')->defaults('posiblesubject', $subject_v)->defaults('hideunhide', 0);
// 	    Route::get('show/'.$subject_v->name, 'ExamController@test')->defaults('subjectid', $subject_v->id);
// 	}
//         } catch(Exception $exception){
// 	$subjects=[];
// 	}

// $subjectroute = ['subject'=>'subject', '/'=>'home'];

// foreach($subjectroute as $subjectroute_k => $subjectroute_v)
// {
//     Route::get(''.$subjectroute_k, 'ExamController@home')->defaults('url', $subjectroute_v);
// }

// Route::middleware('admin')->group(function (){
// 	Route::post('create', 'ExamController@create');
// 	Route::get('addquestion', 'ExamController@add');
// });

// Route::get('check', 'ExamController@check');

// Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


// Route::view('/', 'home');
Route::get('/{any}', function (){
    return view('home');
})->where('any', '.*');
