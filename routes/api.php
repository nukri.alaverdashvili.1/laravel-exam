<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::post('/register', 'ExamController@CreateUser');

Route::post('/login', 'ExamController@Login');

Route::post('/profile', 'ExamController@profile');

Route::post('/question', 'ExamController@question');

Route::post('/hide', 'ExamController@hide');

Route::post('/unhide', 'ExamController@hide');

Route::post('/savetest', 'ExamController@saveTest');

Route::post('/testresults', 'ExamController@testResults');

Route::post('/createquestion', 'ExamController@createQuestion');