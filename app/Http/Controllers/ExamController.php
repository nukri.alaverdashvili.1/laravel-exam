<?php

namespace App\Http\Controllers;

use App\Exceptions\NotFoundException;
use App\Models\Answer;
use App\Models\Question;
use App\Models\Subject;
use App\Models\SubjectUser;
use App\Models\Test;
use App\Models\TestAnswer;
use App\Models\User;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use function PHPSTORM_META\map;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Hash;
use Ixudra\Curl\Facades\Curl;

class ExamController extends Controller
{
    public function home(Request $request, $url)
    {
        $auth = Auth::id();
        $user = User::find($auth);
        if (empty($user)) {
            return view('home');
        }
        $subject = [];
        $subject = $this->filterSubjects($user, $url);
        if ($subject->isEmpty()) {
            return view('home');
        }
        if ($url == 'home') {
            return view('welcome', ['subject' => $subject]);
        } else {
            return view('hidesubject', ['subject' => $subject]);
        }
    }

    public function filterSubjects(User $user, $url)
    {
        if ($url == 'home') {
            $hide = 0;
        } else {
            $hide = 1;
        }
        $user->loadMissing('subject');
        $subject = $user->subject()->where('hide', $hide)->get();
        $subject = $subject->loadMissing('subjectrequired')->filter(function ($subjectfilter) use (&$user) {
            if ($subjectfilter->subjectrequired->isEmpty()) {
                return true;
            } else {
                $deleteSubject = false;
                $subjectfilter->subjectrequired->each(function ($subjectrequired) use (&$user, &$deleteSubject) {
                    $required = $subjectrequired->required_id;

                    $user->subject->each(function ($subjectpass_exam) use (&$required, &$deleteSubject) {
                        if ($subjectpass_exam->id == $required) {
                        } else {
                            return true;
                        }
                        if ($subjectpass_exam->pivot->pass_exam == 1) {
                        } else {
                            $deleteSubject = true;
                            return false;
                        }
                    });
                });
                if ($deleteSubject) {
                    return false;
                } else {
                    return true;
                }
            }
        });
        return $subject;
    }

    public function hideunhide(Request $request, $posiblesubject, $hideunhide)
    {
        $auth = Auth::id();
        $user = User::find($auth);
        $subject = $user->subject()->where('name', $posiblesubject->name)->first();
        $subject->pivot->hide = $hideunhide;
        $subject->pivot->save();
        return redirect('/');
    }

    public function exam(Request $request, $subjectname)
    {
        $auth = Auth::id();
        $user = User::find($auth);
        try{
            $user = User::findOrFail($auth);
        } catch(Exception $exception){
            return view('home');
        }
        $subject = $user->subject()->where('name', $subjectname)->get();
        $subject = $subject->loadMissing('question.answer');
        return view('exam', ['subject' => $subject]);
    }

    public function test(Request $request, $subjectid)
    {
        $auth = Auth::id();
        $user = User::find($auth);
        $method = 'test';

        $testresult = Test::where('user_id', $user->id)->where('subject_id', $subjectid)->get();

        $testresult->loadMissing('testanswer');
        $testresult->loadMissing('testanswer.question');
        $testresult->loadMissing('testanswer.answer');

        return view('showresult', ['result' => $testresult, 'method'=>$method]);
    }

    public function add(Request $request)
    {
        $this->authorize('task');

        $subject = Subject::get();
 
        return view('create',['subject'=>$subject]);
    }

    public function answer(Request $request, $subjectname)
    {
        $subject = Subject::where('name', $subjectname)->first();
        $method = 'answer';

        $param = $request->all();
        unset($param['_token']);

        $test = new Test;
        $test->user_id = Auth::id();
        $test->subject_id = $subject->id;
        $test->save();

        
        $subject->loadMissing('user');
        $subject->loadMissing('question.answer');

        $answerToSave = [];
        foreach ($subject->question as $question_k => $question_v) {
            if (key_exists($question_v->getKey(), $param)) {
                $answerToSave []= [
                    'test_id' => $test->id,
                    'question_id' => $question_v->getKey(),
                    'answer_id' => $param[$question_v->getKey()],
                ];
            }
        }
        TestAnswer::insert($answerToSave);

        $test->loadMissing('testanswer');
        $test->loadMissing('testanswer.question');
        $test->loadMissing('testanswer.answer');

        return view('showresult', ['result' => $test, 'method'=>$method]);
    }

    public function create(Request $request)
    {
        $this->authorize('task');
        $question = new Question;
        $question->subject_id = $request->subject;
        $question->question = $request->question;
        $question->save();
        $correct = $request->correct;
        
        $param = $request->all();
        unset($param['_token']);
        unset($param['subject']);
        unset($param['question']);
        unset($param['correct']);
        unset($param['q']);

        foreach($param as $param_k =>$param_v)
        {
            if($param_v == null)
            {}else{
                $answer = new Answer;
                $answer->question_id = $question->id;
                $answer->answer = $param_v;
                if($param_k == $correct)
                {
                $answer->correct = 1;
                }else{
                $answer->correct = 0;
                }
            }
            $answer->save();
        }

        return redirect('addquestion');
    }

    public function CreateUser(Request $request)
    {
        $user = User::where('email', $request->json('email'))->first();
        if($user)
        {
            return response()->json(['error' =>'User is already taken']);
        }
       $newUser = User::create(['name' => $request->json('name'), 'email' => $request->json('email'), 'password' => Hash::make($request->json('password')),'admin' => 0]);
    //    $token = $newUser->createToken('Token Name')->accessToken;
        return $newUser;
    }

    public function Login(Request $request)
    {
        $user = User::where('email', $request->json('email'))->first();
        if(!$user)
        {
            return response()->json(['error' =>'There is no user in this email']);
        }
        if (!Hash::check($request->json('password'), $user->password)) {
            return response()->json(['error' =>'Password is Incorrect']);
        }
        return $user;
    }

    public function profile(Request $request)
    {
        $user = User::find($request->loginData);
        $user->loadMissing('subject');
        if($request->json('proftype') == 'unhide')
        {
            $subject = $user->subject()->where('hide', 1)->get();
            return $subject;
        }
        $subject = $user->subject()->where('hide', 0)->get();
        return $subject;
    }

    public function question(Request $request)
    {
        $subject = Subject::where('name', $request->subj)->first();
        $subject = $subject->question->loadMissing('answer');
        return $subject;
    }

    public function hide(Request $request){
        if($request->json('proftype') =='hide')
        {
            $SubjectUser = SubjectUser::where('subject_id', $request->json('id'))->where('user_id', $request->json('loginData'))->first();
            $SubjectUser->hide = 1;           
            $SubjectUser->save();
            return response()->json(['correct' =>'Subject is hidden']); 
        }
        $SubjectUser = SubjectUser::where('subject_id', $request->json('id'))->where('user_id', $request->json('loginData'))->first();
        $SubjectUser->hide = 0;
        $SubjectUser->save();
        return response()->json(['correct' =>'Subject is unhide']);
    }

    public function saveTest(Request $request)
    {
        $subject = Subject::where('name', $request->subj)->first();
        $test = Test::create(['user_id'=>$request->json('loginData'), 'subject_id'=>$subject->id]);
        foreach($request->json('values') as $val)
        {
            TestAnswer::create(['test_id'=>$test->id, 'question_id'=>$val['id'], 'answer_id'=> $val['value']])->first();
        }

        $test->loadMissing('testanswer.question');
        $test->loadMissing('testanswer.answer');
        return $test;
    }

    public function testResults(Request $request)
    {
        $subject = Subject::where('name', $request->subj)->first();
        $test = Test::where('user_id', $request->loginData)->where('subject_id', $subject->id)->get();
        $test->loadMissing('testanswer.question');
        $test->loadMissing('testanswer.answer');
        return $test;
    }

    public function createQuestion(Request $request)
    {
        // dd($request);
        $question = Question::create(['subject_id'=> $request->sub, 'question'=> $request->question]);
        $correct = $request->correct;
        $answers = [$request->answer1, $request->answer2, $request->answer3, $request->answer4 ];
        foreach($answers as $answer)
        {
            if(in_array($answer, $correct)){
                Answer::create(['question_id'=> $question->id, 'answer'=>$answer, 'correct'=>1]);
            }else{
                Answer::create(['question_id'=> $question->id, 'answer'=>$answer, 'correct'=>0]);
            }
        }
        return response()->json(['success'=>'Question Added']);
    }

}
