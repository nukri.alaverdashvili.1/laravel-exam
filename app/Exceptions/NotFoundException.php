<?php

namespace App\Exceptions;

use Exception;

class NotFoundException extends Exception
{
    public function report()
    {
    }

    public function render($request)
    {
        return view('error');
    }
}
