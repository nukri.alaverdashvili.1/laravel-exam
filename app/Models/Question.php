<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table = 'question';
    public $fillable = ['subject_id','question'];

    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }

    public function answer()
    {
        return $this->hasMany(Answer::class);
    }
    use HasFactory;
}
