<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $table = 'answer';
    public $fillable = ['question_id','answer', 'correct'];
    
    public function question()
    {
       return $this->belongsTo(Question::class);
    }
    use HasFactory;
}
