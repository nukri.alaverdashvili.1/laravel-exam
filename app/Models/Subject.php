<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $table = 'subject';
    
    public function question()
    {
        return $this->hasMany(Question::class);
    }

    public function test()
    {
        return  $this->hasMany(Test::class);
    }

    public function subjectrequired()
    {
        return $this->hasMany(SubjectRequired::class);
    }

    public function user()
    {
        return $this->belongsToMany(User::class, SubjectUser::class)->withPivot('pass_exam','hide');
    }

    use HasFactory;
}
