<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    protected $table = 'test';
    public $fillable = ['user_id','subject_id'];

    public function subject()
    {
        return $this->belongsTo(Question::class);
    }

    public function user()
    {
        return  $this->belongsTo(User::class);
    }

    public function testanswer()
    {
        return $this->hasMany(TestAnswer::class);
    }
    use HasFactory;
}
