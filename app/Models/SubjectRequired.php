<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubjectRequired extends Model
{
    protected $table = 'subject_required';

    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }

    use HasFactory;
}
